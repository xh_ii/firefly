﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathControl : MonoBehaviour
{
	float speed;
	Vector3[] path;
	float pathLength;
	float distance;
	float endTime;
	float startTime;
	GameObject manager;

	// Use this for initialization
	void Start()
	{
		endTime = -1;
		startTime = 0;
		manager = GameObject.Find("GameManager");

		speed = 1.0f;
		path = iTweenPath.GetPath("Main Path");
		pathLength = iTween.PathLength(path);
		distance = 0.0f;
	}
	
	// Update is called once per frame
	void Update()
	{

		//Debug.Log(distance);
		//Debug.Log(distance / pathLength);
		/*
		if (Input.GetKeyDown(KeyCode.W))
		{
			iTween.Resume(gameObject);
		}
			
		if (Input.GetKeyUp(KeyCode.W))
		{
			iTween.Pause(gameObject);
		}
		*/
		if (startTime < endTime)
		{
			startTime += Time.deltaTime;
			if (startTime >= endTime)
			{
				Debug.Log("ohoh");
				manager.SendMessage("Over");
			}
		}
		else
		{		
			distance += speed * Time.deltaTime;
			if (distance >= pathLength)
			{
				Over();
			}
			else
			{
				iTween.PutOnPath(gameObject, path, distance / pathLength);
			}
		}
	}

	void Over()
	{
		Debug.Log("oh");
		startTime = Time.time;
		endTime = startTime + 5.0f;
	}

	public void SetSpeed(float v)
	{
		speed = v;
	}
}
